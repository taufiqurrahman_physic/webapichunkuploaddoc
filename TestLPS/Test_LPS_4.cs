﻿using System;
using System.Collections.Generic;
namespace TestLPS
{
    public class Test_LPS_4
    {
        public void Answer()
        {
            var myList = new List<Product>();
            while (true)
            {
                // populate list with 1000 integers
                for (int i = 0; i < 1000; i++)
                {
                    myList.Add(new Product(Guid.NewGuid().ToString(), i));
                }
                // do something with the list object
                Console.WriteLine(myList.Count);

                // free the list before re-populate the list
                myList.Clear();
            }
        }
    }
    internal class Product
    {
        public Product(string sku, decimal price)
        {
            SKU = sku;
            Price = price;
        }
        public string SKU { get; set; }
        public decimal Price { get; set; }
    }
}