﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLPS
{
    internal class Test_LPS_7
    {
        public void Answer()
        {
            for (int i = 0; i < 1000000; i++)
            {
                Cache.Add(i, new object(), TimeSpan.FromMinutes(30));
            }
            Console.WriteLine("Cache populated");
            Console.ReadLine();
        }
    }

    class Cache
    {
        private static Dictionary<int, CacheModel> _cache = new Dictionary<int,
       CacheModel>();
        public static void Add(int key, object value, TimeSpan lifeSpan)
        {
            _cache.Add(key,new CacheModel
            {
                Value = value,
                Expiration = DateTime.Now+lifeSpan,
            });
        }
        public static object? Get(int key)
        {
            if(!_cache.ContainsKey(key))
                return null;

            var cacheData = _cache[key];

            if(cacheData == null || cacheData.Expiration < DateTime.Now)
            {
                _cache.Remove(key);
                return null;
            }

            return cacheData.Value;
        }
    }

    internal class CacheModel
    {
        public object Value { get; set; }
        public DateTime Expiration { get; set; }
    }
}
