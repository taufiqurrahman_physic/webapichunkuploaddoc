﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLPS
{
    public class Test_LPS_3
    {
        public void Answer()
        {
            var laptop = new Laptop("macOs");
            Console.WriteLine(laptop.Os); // Laptop os: macOs
        }
    }

    internal class Laptop
    {
        public string Os { get; } // can't be modified
        public Laptop(string os)
        {
            Os = os;
        }
    }
    
}
