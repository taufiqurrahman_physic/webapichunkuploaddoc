﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLPS
{
    public class Test_LPS_6
    {
        public void Answer()
        {
            var rootNode = new TreeNode();
            while (true)
            {
                // create a new subtree of 10000 nodes
                var newNode = new TreeNode();
                for (int i = 0; i < 10000; i++)
                {
                    var childNode = new TreeNode();
                    newNode.AddChild(childNode);
                }
                rootNode.AddChild(newNode);

                //free memory by removing 100 old subtrees
                if(rootNode.Childrens.Count > 1000)
                    rootNode.RemoveRangeChild(100);
            }
        }
    }

    class TreeNode
    {
        private readonly List<TreeNode> _children = new List<TreeNode>();
        public IReadOnlyList<TreeNode> Childrens => _children;
        public void AddChild(TreeNode child)
        {
            _children.Add(child);
        }

        public void RemoveRangeChild(int count)
        {
            _children.RemoveRange(0,count);
        }
    }
}
