﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLPS
{
    public class Test_LPS_1
    {
        public DateTime? CleanAndEasier(Application application)
        {
            return application?.Protected?.ShieldLastRun;
        }
    }

    public class Application
    {
        public ProtectedInfo Protected { get; } = new ProtectedInfo();
    }

    public class ProtectedInfo
    {
        public DateTime ShieldLastRun { get; set; }
    }
}
