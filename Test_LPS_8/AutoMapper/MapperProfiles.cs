﻿using AutoMapper;
using Models;
using Test_LPS_8.Entity;
using Test_LPS_8.Models.DataViewModels;
using ViewModel;

namespace AccountManajement.AutoMapper
{
    public class MapperProfiles : Profile
    {
        public MapperProfiles() 
        {
            CreateMap<FileManagerViewModel, FileMetadata>()
                .ReverseMap();

            CreateMap<AccountViewModel, Account>()
                .ReverseMap();

            CreateMap<AuthenticationResponse, Account>()
                .ReverseMap()
                 .ForMember(dest => dest.Username, opt => opt.MapFrom(s => s.Username));
        }
    }
}
