﻿using AutoMapper;
using MediatR;
using Test_LPS_8.MediatR.Command;
using Test_LPS_8.MediatR.RequestModel.Query.FileManager;
using Test_LPS_8.Models.DataViewModels;
using Test_LPS_8.Repository.Interface.ReadRepository;
using ViewModel;

namespace Test_LPS_8.MediatR.Query
{
    public class FileManagerQueryHandler : IRequestHandler<GetFileMetadatas, IEnumerable<FileManagerViewModel>>,
                                          IRequestHandler<GetFileMetadataById, FileManagerViewModel>
    {
        private readonly IMapper _mapper;
        private readonly IFileManagerQuery _fileManagerQuery;

        public FileManagerQueryHandler(IMapper mapper,
                                        IFileManagerQuery fileManagerQuery)
        {
            _mapper = mapper;
            _fileManagerQuery = fileManagerQuery;
        }

        public async Task<IEnumerable<FileManagerViewModel>> Handle(GetFileMetadatas request, CancellationToken cancellationToken)
        {
            var datas = await _fileManagerQuery.GetAll();

            if (datas == null)
            {
                return null;
            }

            return datas.Select(e => _mapper.Map<FileManagerViewModel>(e));
        }

        public async Task<FileManagerViewModel> Handle(GetFileMetadataById request, CancellationToken cancellationToken)
        {
            var data = await _fileManagerQuery.GetFileById(request.Id);

            if (data == null)
                return null;

            var result = _mapper.Map<FileManagerViewModel>(data);

            return result;
        }
    }
}
