﻿using AutoMapper;
using Azure.Core;
using JwtAuthenticationManager;
using MediatR.RequestModel.Query.Employe;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Models;
using Repository.Interface.CommandRepository;
using Repository.Interface.ReadRepository;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Test_LPS_8.MediatR.RequestModel.Query.Account;
using Test_LPS_8.Models.ResponseViewModels;
using ViewModel;

namespace MediatR.Query
{
    public class AccountQueryHandler : IRequestHandler<GetAllAccounts, IEnumerable<AccountViewModel>>,
                                       IRequestHandler<GetAccountById, AccountViewModel>,
                                       IRequestHandler<GetAuthenticationAccount, AuthenticationResponse>,
                                       IRequestHandler<GetNewRefreshToken, TokenViewModels>
    {
        private readonly IAccountQuery _repository;
        private readonly IAccountCommand _commandRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public AccountQueryHandler(IAccountQuery repository,
                                    IMapper mapper,
                                    IConfiguration configuration,
                                    IAccountCommand accountCommand)
        {
            _repository = repository;
            _commandRepository = accountCommand;
            _mapper = mapper;
            _configuration = configuration;

        }

        public async Task<IEnumerable<AccountViewModel>> Handle(GetAllAccounts request, CancellationToken cancellationToken)
        {
            var datas = await _repository.GetAccounts();

            if (datas == null)
            {
                return null;
            }

            return datas.Select(e=>_mapper.Map<AccountViewModel>(e));
        }

        public async Task<AccountViewModel> Handle(GetAccountById request, CancellationToken cancellationToken)
        {
            var data = await _repository.GetEmploye(request.Id);

            if(data == null)
            {
                return null;
            }

            var result = _mapper.Map<AccountViewModel>(data);

            return result;
        }

        public async Task<AuthenticationResponse> Handle(GetAuthenticationAccount request, CancellationToken cancellationToken)
        {
            var user = await _repository.Authentication(request.Username, request.Password);

            if (user == null)
            {
                return null;
            }

            var result = _mapper.Map<AuthenticationResponse>(user);
            
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var jwtTokenHandler = new JwtTokenHandler(_configuration);
            var securityToken = jwtTokenHandler.CreateToken(claims);

            var token = new JwtSecurityTokenHandler().WriteToken(securityToken);
            var refreshToken = jwtTokenHandler.GenerateRefreshToken();

            _ = int.TryParse(_configuration["JWT:RefreshTokenValidityInDays"], out int refreshTokenValidityInDays);

            user.RefreshToken = refreshToken;
            user.RefreshTokenExipryTime = DateTime.Now.AddDays(refreshTokenValidityInDays);

            await _commandRepository.Update(user);

            result.JwtToken = token;
            result.JwtRefreshToken = refreshToken;
            result.ExpiresIn = (int)securityToken.ValidTo.Subtract(DateTime.Now).TotalSeconds;

            return result;
        }

        public async Task<TokenViewModels> Handle(GetNewRefreshToken request, CancellationToken cancellationToken)
        {
            var tokenHandler = new JwtTokenHandler(_configuration);
            var principal = tokenHandler.GetPrincipalFromExpiredToken(request.AccessToken);

            if (principal == null)
                return null;

            string username = principal.Identity.Name;

            var user = await _repository.GetAccountByUsername(username);

            if (user == null || user.RefreshToken != request.RefreshToken || user.RefreshTokenExipryTime <= DateTime.Now)
            {
                return null;
            }

            var newAccessToken = tokenHandler.CreateToken(principal.Claims.ToList());
            var newRefreshToken = tokenHandler.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;

            await _commandRepository.Update(user);

            return new TokenViewModels
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(newAccessToken),
                RefreshToken = newRefreshToken,
            };
        }
    }
}
