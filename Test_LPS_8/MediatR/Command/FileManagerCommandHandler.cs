﻿using JwtAuthenticationManager;
using MediatR;
using Microsoft.Extensions.Configuration;
using Repository.Interface.ReadRepository;
using Test_LPS_8.Entity;
using Test_LPS_8.MediatR.RequestModel.Command.FileManager;
using Test_LPS_8.Models;
using Test_LPS_8.Repository.Interface.CommandRepository;

namespace Test_LPS_8.MediatR.Command
{
    public class FileManagerCommandHandler : IRequestHandler<UploadFile, (bool, string)>
    {
        private readonly IConfiguration _configuration;
        private readonly IAccountQuery _accountQuery;
        private readonly IFileManagerCommand _fileManagerCommand;

        public FileManagerCommandHandler(IConfiguration configuration, 
                                         IAccountQuery accountQuery,
                                         IFileManagerCommand fileManagerCommand)
        {
            _configuration = configuration;
            _accountQuery = accountQuery;
            _fileManagerCommand = fileManagerCommand;
        }

        public async Task<(bool, string)> Handle(UploadFile request, CancellationToken cancellationToken)
        {
            var tokenHandler = new JwtTokenHandler(_configuration);
            var principal = tokenHandler.GetPrincipalFromExpiredToken(request.AccessToken);

            if (principal == null)
                return (false,"Invalid Access Token.");

            string username = principal.Identity.Name;

            var user = await _accountQuery.GetAccountByUsername(username);

            if (user == null)
            {
                return (false,"Invalid Access Token.");
            }

            var tempFilePath = Path.GetTempFileName();

            using (var fileStream = new FileStream(tempFilePath, FileMode.Create))
            {
                await request.File.CopyToAsync(fileStream);
            }

            string destinationDirectory = _configuration["File:Url"];

            if (!Directory.Exists(destinationDirectory))
            {
                Directory.CreateDirectory(destinationDirectory);
            }

            string fileName = Path.GetFileName(tempFilePath);
            // Construct the destination file path by combining the destination directory and the original file name
            string destinationFilePath = Path.Combine(destinationDirectory, fileName);

            // Move the file from the temporary location to the destination directory
            File.Move(tempFilePath, destinationFilePath);

            if(File.Exists(tempFilePath))
                File.Delete(tempFilePath);

            //save the data
            var dataSaved = await _fileManagerCommand.CreateFileUploaded(new FileMetadata
            {
                AccountId = user.Id,
                ContentType = request.File.ContentType,
                FileName = fileName,
                Size = request.File.Length,
                FilePath = destinationFilePath
            });

            if (dataSaved == null)
                return (false, "Failed saved file data");

            return (true, "Document is submitted successfully.");
        }
    }
}
