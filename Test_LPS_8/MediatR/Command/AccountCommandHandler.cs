﻿using AutoMapper;
using MediatR.RequestModel.Command.Employe;
using Repository.Interface.CommandRepository;
using Test_LPS_8.Entity;
using ViewModel;

namespace MediatR.Query
{
    public class AccountCommandHandler : IRequestHandler<CreateAccount, AccountViewModel>,
                                        IRequestHandler<UpdateAccount, AccountViewModel>,
                                        IRequestHandler<DeleteAccount, AccountViewModel>
    {
        private readonly IAccountCommand _repository;
        private readonly IMapper _mapper;

        public AccountCommandHandler(IAccountCommand repository,
                                    IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<AccountViewModel> Handle(CreateAccount request, CancellationToken cancellationToken)
        {
            var data = await _repository.Create((Account)request);

            if (data == null) return null;

            var result = _mapper.Map<AccountViewModel>(data);

            return result;
        }

        public async Task<AccountViewModel> Handle(UpdateAccount request, CancellationToken cancellationToken)
        {
            var data = await _repository.Update((Account)request);

            if (data == null) return null;

            var result = _mapper.Map<AccountViewModel>(data);

            return result;
        }

        public async Task<AccountViewModel> Handle(DeleteAccount request, CancellationToken cancellationToken)
        {
            var data = await _repository.Delete(request.Id);

            if (data == null) return null;

            var result = _mapper.Map<AccountViewModel>(data);

            return result;
        }
    }
}
