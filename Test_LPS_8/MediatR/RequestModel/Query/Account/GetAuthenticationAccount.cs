﻿using MediatR;
using Models;
using Test_LPS_8.Entity;

namespace MediatR.RequestModel.Query.Employe
{
    public class GetAuthenticationAccount : Account, IRequest<AuthenticationResponse>
    {
        public GetAuthenticationAccount(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }
    }
}
