﻿using MediatR;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Test_LPS_8.Models.ResponseViewModels;

namespace Test_LPS_8.MediatR.RequestModel.Query.Account
{
    public class GetNewRefreshToken : TokenViewModels, IRequest<TokenViewModels>
    {
        public GetNewRefreshToken(string accessToken ,string refreshToken) 
        { 
            this.AccessToken = accessToken;
            this.RefreshToken = refreshToken;
        }
    }
}
