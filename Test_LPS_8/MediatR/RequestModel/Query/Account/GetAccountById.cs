﻿using MediatR;
using ViewModel;

namespace MediatR.RequestModel.Query.Employe
{
    public class GetAccountById : IRequest<AccountViewModel>
    {
        public string Id { get; set; }

        public GetAccountById(string id)
        {
            Id = id;
        }
    }
}
