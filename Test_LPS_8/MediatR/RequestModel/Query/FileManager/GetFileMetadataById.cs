﻿using MediatR;
using Test_LPS_8.Models.DataViewModels;

namespace Test_LPS_8.MediatR.RequestModel.Query.FileManager
{
    public class GetFileMetadataById : IRequest<FileManagerViewModel>
    {
        public string Id { get; set; }
        public GetFileMetadataById(string id)
        {
            Id = id;
        }
    }
}
