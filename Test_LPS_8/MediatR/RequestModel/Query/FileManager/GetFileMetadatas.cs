﻿using MediatR;
using Test_LPS_8.Models.DataViewModels;

namespace Test_LPS_8.MediatR.RequestModel.Query.FileManager
{
    public class GetFileMetadatas : IRequest<IEnumerable<FileManagerViewModel>>
    {

    }
}
