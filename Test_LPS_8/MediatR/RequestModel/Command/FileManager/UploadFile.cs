﻿using MediatR;
using Test_LPS_8.Models;

namespace Test_LPS_8.MediatR.RequestModel.Command.FileManager
{
    public class UploadFile : FileManagerModels, IRequest<(bool,string)>
    {
        public UploadFile(IFormFile file, string accessToken) 
        {
            this.File = file;
            this.AccessToken = accessToken;
        }
    }
}
