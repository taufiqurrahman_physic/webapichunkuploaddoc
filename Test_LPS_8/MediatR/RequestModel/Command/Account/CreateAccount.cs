﻿using Test_LPS_8.Entity;
using ViewModel;

namespace MediatR.RequestModel.Command.Employe
{
    public class CreateAccount : Account, IRequest<AccountViewModel>
    {
        public CreateAccount(string username, string password, string role)
        {
            this.Username = username;
            this.Password = password;
            this.Role = role;
        }
    }
}
