﻿using MediatR;
using Test_LPS_8.Entity;
using ViewModel;

namespace MediatR.RequestModel.Command.Employe
{
    public class UpdateAccount : Account, IRequest<AccountViewModel>
    {
        public UpdateAccount(string id, string username, string password, string role)
        {
            Id = id;
            Username = username;
            Password = password;
            Role = role;
        }
    }
}
