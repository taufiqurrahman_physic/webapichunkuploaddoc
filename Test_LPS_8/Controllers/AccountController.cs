﻿using MediatR;
using MediatR.RequestModel.Command.Employe;
using MediatR.RequestModel.Query.Employe;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.ViewModels;
using Test_LPS_8.MediatR.RequestModel.Query.Account;
using Test_LPS_8.Models.ResponseViewModels;
using Test_LPS_8.Services.Extension;
using ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AccountManajement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        public AccountController(IMediator mediator, ILogger<AccountController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        // GET: api/<AccountController>
        [HttpGet]
      //  [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<ListResponseViewModel<AccountViewModel>> Get()
        {
            _logger.LogInformation("Fetch all data of Account");

            var datas = await _mediator.Send(new GetAllAccounts());

            var result = new ListResponseViewModel<AccountViewModel>();
            
            if (datas == null)
            {
                _logger.LogInformation("No data of Accounts");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Datas = new List<AccountViewModel>();

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Datas = datas;

            return result;
        }

        // GET api/<AccountController>/5
        [HttpGet("{id}")]
        //[Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<AccountViewModel>> Get(string id)
        {
            var result = new SingleResponseViewModel<AccountViewModel>();

            if (string.IsNullOrEmpty(id))
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Fetch particular Account for this id = "+id);

            var data = await _mediator.Send(new GetAccountById(id));

            if(data == null)
            {
                _logger.LogInformation("No data of Account for this id = "+id+" found.");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Data = data;

            return result;
        }

        // POST api/<AccountController>
        [HttpPost("Register")]
        public async Task<SingleResponseViewModel<AccountViewModel>> Post([FromBody] BaseAccountViewModel data)
        {
            var result = new SingleResponseViewModel<AccountViewModel>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter \\is null";
                result.Data = null;

                return result;
            }

            if (!ModelState.IsValid)
            {
                _logger.LogInformation("Data parameter is is not valid");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is is not valid";
                result.Data = null;

                return result;
            }

            if (!data.Password.ValidatePassword())
            {
                _logger.LogInformation("Password not match with the requierement.");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Password has wrong format";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to insert the data");

            var response = await _mediator.Send(new CreateAccount(
                data.Username,
                data.Password,
                data.Role
                ));

            if (response == null)
            {
                _logger.LogInformation("No data of Account created");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data created";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "New Record Created.";
            result.Data = response;

            return result;

        }

        // PUT api/<AccountController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<AccountViewModel>> Put(string id, [FromBody] BaseAccountViewModel data)
        {
            var result = new SingleResponseViewModel<AccountViewModel>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            if (!ModelState.IsValid)
            {
                _logger.LogInformation("Data parameter is is not valid");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is is not valid";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to update the data");

            var response = await _mediator.Send(new UpdateAccount(
                id,
                data.Username,
                data.Password,
                data.Role
                ));

            if (response == null)
            {
                _logger.LogInformation("No data of Account updated");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data updated";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Data updated.";
            result.Data = response;

            return result;
        }

        // DELETE api/<AccountController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoleName.Admin)]
        public async Task<SingleResponseViewModel<AccountViewModel>> Delete(string id)
        {
            var result = new SingleResponseViewModel<AccountViewModel>();

            if (string.IsNullOrEmpty(id))
            {
                _logger.LogInformation("Id parameter is empty or null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Parameter id is empty or null";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to delete the data");

            var response = await _mediator.Send(new DeleteAccount(
            id));

            if (response == null)
            {
                _logger.LogInformation("No data of Account deleted");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data daleted";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "This data removed from database.";
            result.Data = response;

            return result;
        }

        //Login
        [HttpPost("Login")]
        public async Task<SingleResponseViewModel<AuthenticationResponse>> Authentication([FromBody] AuthenticationRequest data)
        {
            var result = new SingleResponseViewModel<AuthenticationResponse>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            if (!data.Password.ValidatePassword())
            {
                _logger.LogInformation("Password not match with the requierement.");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Password has wrong format";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to insert the data");

            var response = await _mediator.Send(new GetAuthenticationAccount(data.Username,data.Password));

            if (response == null)
            {
                _logger.LogInformation("Login Failed, Account not found.");

                result.StatusCode = StatusCodes.Status401Unauthorized;
                result.Message = "Authorization Process Failed";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Authorized.";
            result.Data = response;

            return result;

        }

        [HttpPost]
        [Route("refresh-token")]
        public async Task<SingleResponseViewModel<TokenViewModels>> RefreshToken(TokenViewModels data)
        {
            var result = new SingleResponseViewModel<TokenViewModels>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            var getNewAccessToken = await _mediator.Send(new GetNewRefreshToken(data.AccessToken, data.RefreshToken));

            if (getNewAccessToken == null)
            {
                _logger.LogInformation("Failed create new access token");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Invalid refresh token.";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "New access token created.";
            result.Data = getNewAccessToken;

            return result;
        }
    }
}
