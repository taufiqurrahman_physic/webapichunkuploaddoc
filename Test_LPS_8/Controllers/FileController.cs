﻿using AccountManajement.Controllers;
using MediatR;
using MediatR.RequestModel.Query.Employe;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Models.ViewModels;
using Test_LPS_8.MediatR.RequestModel.Command.FileManager;
using Test_LPS_8.MediatR.RequestModel.Query.FileManager;
using Test_LPS_8.Models.DataViewModels;
using ViewModel;

namespace Test_LPS_8.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FileController : ControllerBase
    {
        private const long _minFileSizeBytes = 1024 * 1024 * 1024;

        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public FileController(IMediator mediator, ILogger<AccountController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost("upload")]
        public async Task<SingleResponseViewModel<string>> UploadFile(IFormFile file)
        {
            var result = new SingleResponseViewModel<string>();

            if (file == null || file.Length == 0)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "No file uploaded";

                return result;
            }

            if (file.Length < _minFileSizeBytes)
            {
                _logger.LogInformation("file length less than 3 GB");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "File size must have the minimum allowed size of 1 GB.";

                return result;
            }

            var accessToken = HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            var response = await _mediator.Send( new UploadFile(file, accessToken));

            if (!response.Item1)
            {
                _logger.LogInformation("failed upload and save file");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = response.Item2;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = response.Item2;

            return result;
        }

        [HttpGet("{id}")]
        //[Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<FileManagerViewModel>> Get(string id)
        {
            var result = new SingleResponseViewModel<FileManagerViewModel>();

            if (string.IsNullOrEmpty(id))
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Fetch particular file for this id = " + id);

            var data = await _mediator.Send(new GetFileMetadataById(id));

            if (data == null)
            {
                _logger.LogInformation("No data of Document for this id = " + id + " found.");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Data = data;

            return result;
        }

        [HttpGet]
        //  [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<ListResponseViewModel<FileManagerViewModel>> Get()
        {
            _logger.LogInformation("Fetch all data of Account");

            var datas = await _mediator.Send(new GetFileMetadatas());

            var result = new ListResponseViewModel<FileManagerViewModel>();

            if (datas == null)
            {
                _logger.LogInformation("No data of Accounts");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Datas = new List<FileManagerViewModel>();

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Datas = datas;

            return result;
        }

        [HttpGet("Download/{id}")]
        public async Task<IActionResult> DownloadFile(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new JsonResult( new SingleResponseViewModel<string>
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Message = "Document Id is empty or null"
                });
            }

            var documentData = await _mediator.Send(new GetFileMetadataById(id));

            if(documentData == null)
                return new JsonResult(new SingleResponseViewModel<string>
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Message = "Document data is not found"
                });

            // Check if the file exists
            if (!System.IO.File.Exists(documentData.FilePath))
            {
                return new JsonResult(new SingleResponseViewModel<string>
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Message = "Document not found."
                });
            }

            // Set the Content-Disposition header to indicate a file attachment
            Response.Headers.Add("Content-Disposition", $"attachment; filename=\"{documentData.FileName}\"");

            // Stream the file to the response stream
            var stream = new FileStream(documentData.FilePath, FileMode.Open, FileAccess.Read);
            return File(stream, "application/octet-stream");
        }
    }
}
