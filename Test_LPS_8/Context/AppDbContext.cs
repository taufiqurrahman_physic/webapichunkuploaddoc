﻿using Microsoft.EntityFrameworkCore;
using Test_LPS_8.Entity;

namespace Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) 
        {

        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<FileMetadata> FileMetadatas { get; set; }
    }
}
