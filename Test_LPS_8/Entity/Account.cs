﻿using System.ComponentModel.DataAnnotations;

namespace Test_LPS_8.Entity
{
    public class Account
    {
        [Key]
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string? RefreshToken { get; set; }
        public DateTime? RefreshTokenExipryTime { get; set; }
        public ICollection<FileMetadata> Files { get; set; }
    }
}
