﻿using System.ComponentModel.DataAnnotations;

namespace Test_LPS_8.Entity
{
    public class FileMetadata
    {
        [Key]
        public string Id { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public long Size { get; set; }
        public string FilePath { get; set; }
        public string AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}
