﻿namespace Test_LPS_8.Models
{
    public class FileManagerModels
    {
        public IFormFile File { get; set; }
        public string AccessToken { get; set; }
    }
}
