﻿namespace Test_LPS_8.Models.ResponseViewModels
{
    public class TokenViewModels
    {
        public string? AccessToken { get; set; }
        public string? RefreshToken { get; set; }
    }
}
