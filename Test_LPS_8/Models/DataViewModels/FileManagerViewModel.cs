﻿using Test_LPS_8.Entity;
using ViewModel;

namespace Test_LPS_8.Models.DataViewModels
{
    public class FileManagerViewModel
    {
        public string Id { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public long Size { get; set; }
        public string FilePath { get; set; }
        public string AccountId { get; set; }
        public AccountViewModel Account { get; set; }
    }
}
