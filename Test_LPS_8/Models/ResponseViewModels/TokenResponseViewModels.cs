﻿namespace Test_LPS_8.Models.ResponseViewModels
{
    public class TokenResponseViewModels
    {
        public string? AccessToken { get; set; }
        public string? RefreshToken { get; set; }
    }
}
