﻿
using Repository.Interface.CommandRepository;
using Repository.Interface.ReadRepository;
using Repository.Service.CommandRepository;
using Repository.Service.ReadRepository;
using Test_LPS_8.Repository.Interface.CommandRepository;
using Test_LPS_8.Repository.Interface.ReadRepository;
using Test_LPS_8.Repository.Service.CommandRepository;
using Test_LPS_8.Repository.Service.ReadRepository;

namespace Extension
{
    public static class ServiceResolver
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IAccountCommand, AccountCommand>();
            services.AddScoped<IAccountQuery, AccountQuery>();
            services.AddScoped<IFileManagerCommand, FileManagerCommand>();
            services.AddScoped<IFileManagerQuery, FileManagerQuery>();
        }
    }
}
