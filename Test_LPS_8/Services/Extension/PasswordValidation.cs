﻿using System.Text.RegularExpressions;

namespace Test_LPS_8.Services.Extension
{
    public static class PasswordValidation
    {
        public static bool ValidatePassword(this string password)
        {
            // Define regular expressions for each condition
            var lowercaseRegex = new Regex(@"[a-z]");
            var uppercaseRegex = new Regex(@"[A-Z]");
            var specialCharRegex = new Regex(@"[!@#$%^&*()-+]");
            var digitRegex = new Regex(@"\d");

            // Check each condition
            bool hasLowercase = lowercaseRegex.IsMatch(password);
            bool hasUppercase = uppercaseRegex.IsMatch(password);
            bool hasSpecialChar = specialCharRegex.IsMatch(password);
            bool hasDigit = digitRegex.IsMatch(password);
            bool hasMinimumLength = password.Length >= 8;

            // Validate the password against all conditions
            return hasLowercase && hasUppercase && hasSpecialChar && hasDigit && hasMinimumLength;
        }
    }
}
