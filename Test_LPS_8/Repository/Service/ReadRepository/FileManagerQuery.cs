﻿using Context;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Test_LPS_8.Entity;
using Test_LPS_8.Repository.Interface.ReadRepository;

namespace Test_LPS_8.Repository.Service.ReadRepository
{
    public class FileManagerQuery : IFileManagerQuery
    {
        private readonly DapperContext _context;

        public FileManagerQuery(DapperContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<FileMetadata>> GetAll()
        {
            var query = "SELECT * FROM FileMetadatas";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<FileMetadata>(query);
                return result.ToList();
            }
        }

        public async Task<FileMetadata> GetFileById(string id)
        {
            var query = "SELECT * FROM FileMetadatas WHERE Id = @Id";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<FileMetadata>(query, new { id });

                return result;
            }
        }
    }
}
