﻿using Context;
using Dapper;
using Repository.Interface.ReadRepository;
using Test_LPS_8.Entity;

namespace Repository.Service.ReadRepository
{
    public class AccountQuery : IAccountQuery
    {
        private readonly DapperContext _context;
        public AccountQuery(DapperContext context)
        {
            _context = context;
        }
        public async Task<Account> GetEmploye(string id)
        {
            var query = "SELECT * FROM Accounts WHERE Id = @Id";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<Account>(query, new { id });

                return result;
            }
        }

        public async Task<IEnumerable<Account>> GetAccounts()
        {
            var query = "SELECT * FROM Accounts";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<Account>(query);
                return result.ToList();
            }
        }

        public async Task<Account> Authentication(string username, string password)
        {
            var query = "SELECT * FROM Accounts WHERE Username = @username AND Password = @password";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<Account>(query, new { username =  username, password = password});

                return result;
            }
        }

        public async Task<Account> GetAccountByUsername(string username)
        {
            var query = "SELECT * FROM Accounts WHERE Username = @username";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<Account>(query, new { username = username });

                return result;
            }
        }
    }
}
