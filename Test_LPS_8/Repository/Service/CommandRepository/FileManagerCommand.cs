﻿using Context;
using Test_LPS_8.Entity;
using Test_LPS_8.Repository.Interface.CommandRepository;

namespace Test_LPS_8.Repository.Service.CommandRepository
{
    public class FileManagerCommand : IFileManagerCommand
    {
        private readonly AppDbContext _context;

        public FileManagerCommand(AppDbContext context)
        {
            _context = context;
        }

        public async Task<FileMetadata> CreateFileUploaded(FileMetadata data)
        {
            if (data == null)
                return null;

            data.Id = Guid.NewGuid().ToString();
            await _context.AddAsync(data);
            await _context.SaveChangesAsync();

            return data;
        }
    }
}
