﻿using Test_LPS_8.Entity;
using Test_LPS_8.Models.DataViewModels;

namespace Test_LPS_8.Repository.Interface.ReadRepository
{
    public interface IFileManagerQuery
    {
        Task<IEnumerable<FileMetadata>> GetAll();
        Task<FileMetadata> GetFileById(string id);
    }
}
