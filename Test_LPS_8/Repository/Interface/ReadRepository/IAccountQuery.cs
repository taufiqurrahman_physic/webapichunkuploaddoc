﻿
using Test_LPS_8.Entity;

namespace Repository.Interface.ReadRepository
{
    public interface IAccountQuery
    {
        Task<IEnumerable<Account>> GetAccounts();
        Task<Account> GetEmploye(string id);
        Task<Account> Authentication(string username,string password);
        Task<Account> GetAccountByUsername(string username);
    }
}
