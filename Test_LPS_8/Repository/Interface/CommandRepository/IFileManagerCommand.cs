﻿using Test_LPS_8.Entity;

namespace Test_LPS_8.Repository.Interface.CommandRepository
{
    public interface IFileManagerCommand
    {
        Task<FileMetadata> CreateFileUploaded(FileMetadata data);
    }
}
